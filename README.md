# HSC Stats #

This is a set of python scripts to pull data from the Higher School Certificate (http://www.boardofstudies.nsw.edu.au/bos_stats/).

### What is this repository for? ###

* Download enrollment data for each course in the HSC since 2001 
* Produce a few plot showing trends in enrolment since 2001

### How do I get set up? ###

* Just download the scripts and run them in python >=2.6.
* There is a dependency on a package I developed to write nice csv files and perform linear regressions. The package is not public yet. Will provide it on demand.


### Who do I talk to? ###

* Julien Lerat (julien.lerat@gmail.com)