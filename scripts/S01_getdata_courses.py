#!/usr/bin/env python

# -- Script Meta Data --
# Author : J. Lerat, EHP, Bureau of Meteorogoloy
# Versions :
#    V00 - Script written from template on 2016-03-01 21:26:09.878951
#
# ------------------------------


from datetime import datetime
time_now = datetime.now
print('\n\n## Script run started at {0} ##\n\n'.format(time_now()))

import sys, os, re, json, math, subprocess

from itertools import product

import urllib2
from urllib2 import HTTPError
from bs4 import BeautifulSoup

import numpy as np
import pandas as pd

from hyio import csv
from hystat.linreg import Linreg

#------------------------------------------------------------
# Options
#------------------------------------------------------------

hsc_url_template = 'https://www.boardofstudies.nsw.edu.au/ebos/static'

year_start = 2001
year_end = 2015

#------------------------------------------------------------
# Folders
#------------------------------------------------------------

source_file = os.path.abspath(__file__)

FROOT = os.path.join(os.path.dirname(source_file), '..')

FDATA = os.path.join(FROOT, 'data')
if not os.path.exists(FDATA): os.mkdir(FDATA)

#------------------------------------------------------------
# Get data
#------------------------------------------------------------

data_all = []
count_all = []

for year in range(year_start, year_end+1):

    # Get HSC total entry
    url = hsc_url_template + '/TOTCN_' + str(year) + '_12.html'

    try:
        web = urllib2.urlopen(url)
        ok = True
    except HTTPError:
        ok = False

    if ok:
        soup = BeautifulSoup(web.read())
        div = soup.find('div', {'id':'cim_main-content'})
        table = div.find_all('table')[-1]
        data = {}
        for row in table.find_all('tr'):
            col = row.find_all('td')
            if len(col) == 4:
                item = col[0].string.strip()
                nMale = int(re.sub(',', '', col[1].string.strip()))
                nFemale = int(re.sub(',', '', col[2].string.strip()))
                nTotal = int(re.sub(',', '', col[3].string.strip()))
                data[item] = {'Male':nMale, 'Female':nFemale, 'Total':nTotal}

        data = pd.DataFrame(data)
        data['year'] = year
        count_all.append(data)

    # Get HSC entry by course
    url = hsc_url_template + '/EN_SX_' + str(year) + '_12.csv'
    ftmp = os.path.join(FDATA, 'tmp.csv')
    cmd = 'wget {0} -O {1}'.format(url, ftmp)
    subprocess.check_call(cmd, shell=True)

    data = pd.read_csv(ftmp)
    data['year'] = year
    data = data.rename(columns = {
            'Course number': 'Course Number',
            'Unnamed: 0': 'Course Number',
            'Unnamed: 6': 'Total - Female',
            'Unnamed: 5': 'Total - Male',
            'Unnamed: 7': 'Total - Total',
        })
    os.remove(ftmp)

    #comment = {'comment':'HSC statistics', 'year':year, 'downloaded_from':url}
    #fcsv = os.path.join(FDATA, 'HSC_stats_{0}.csv'.format(year))
    #csv.write_csv(data, fcsv, comment, source_file)

    data_all.append(data)


data_all = pd.concat(data_all)
comment = 'HSC statistics from {0}'.format(hsc_url_template)
fcsv = os.path.join(FDATA, 'HSC_stats_{0}-{1}.csv'.format(year_start,
                                                    year_end))
csv.write_csv(data_all, fcsv, comment, source_file)


# Gap filling 2002 values
count = pd.concat(count_all)
count.index.name = 'gender'
count = count.reset_index()
count = pd.melt(count, id_vars = ['year', 'gender'])

yy = pd.Series(range(year_start, year_end+1))

for (v, g) in product(count['variable'].unique(),
        count['gender'].unique()):

    idx = (count['variable'] == v) & (count['gender'] == g)
    data = count[idx].sort('year')

    x = data['year'].values
    miss = yy[~yy.isin(x)].values

    y = data.loc[:, 'value'].values
    lm = Linreg(x, y)
    lm.fit()
    yhat, _ = lm.predict(x0=miss)

    for i, y in enumerate(miss):
        count = count.append({'year':y, 'variable':v, 'gender':g,
            'value':yhat[i]}, ignore_index=True)

count = count.sort(['year', 'gender', 'variable'])

comment = 'HSC head count from {0}'.format(hsc_url_template)
fcsv = os.path.join(FDATA, 'HSC_headcount_{0}-{1}.csv'.format(year_start,
                                                    year_end))
csv.write_csv(count, fcsv, comment,
        source_file, compress=False)



print('\n\n## Script run completed at {0} ##\n\n'.format(time_now()))
