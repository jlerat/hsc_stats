#!/usr/bin/env python

# -- Script Meta Data --
# Author : J. Lerat, EHP, Bureau of Meteorogoloy
# Versions :
#    V00 - Script written from template on 2016-03-01 21:26:09.878951
#
# ------------------------------


from datetime import datetime
time_now = datetime.now
print('\n\n## Script run started at {0} ##\n\n'.format(time_now()))

import sys, os, re, json, math, subprocess

from itertools import product

import urllib2
from urllib2 import HTTPError
from bs4 import BeautifulSoup

import numpy as np
import pandas as pd

from hyio import csv
from hystat.linreg import Linreg

#------------------------------------------------------------
# Options
#------------------------------------------------------------

url = 'http://www.boardofstudies.nsw.edu.au/syllabus_hsc/course-descriptions'

#------------------------------------------------------------
# Folders
#------------------------------------------------------------

source_file = os.path.abspath(__file__)

FROOT = os.path.join(os.path.dirname(source_file), '..')

FDATA = os.path.join(FROOT, 'data')
if not os.path.exists(FDATA): os.mkdir(FDATA)

#------------------------------------------------------------
# Get data
#------------------------------------------------------------

web = urllib2.urlopen(url)

soup = BeautifulSoup(web.read())
div = soup.find('div', {'id':'cim_main-content'})

syllabus = div.find_all('h3')
courses = div.find_all('ul')

syl = []
for i, s in enumerate(syllabus):
    nm = s.string.strip()

    if nm.startswith('Life Skills'):
        syl.append({'syllabus':nm, 'course':'Life Skills'})
    else:
        for li in courses[i].find_all('li'):
            cnm = re.sub('Heritage Language ', 'Languages Heritage ', li.string.strip())
            syl.append({'syllabus':nm, 'course': cnm})

syl = pd.DataFrame(syl)
comment = 'HSC syllabus from {0}'.format(url)

fsyl = os.path.join(FDATA, 'HSC_syllabus.csv')
csv.write_csv(syl, fsyl, comment, source_file, compress=False)



print('\n\n## Script run completed at {0} ##\n\n'.format(time_now()))
