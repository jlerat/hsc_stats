#!/usr/bin/env python

# -- Script Meta Data --
# Author : J. Lerat, EHP, Bureau of Meteorogoloy
# Versions :
#    V00 - Script written from template on 2016-03-01 21:26:09.878951
#
# ------------------------------


from datetime import datetime
time_now = datetime.now
print('\n\n## Script run started at {0} ##\n\n'.format(time_now()))

import sys, os, re, json, math, subprocess

import numpy as np
import pandas as pd

from fuzzywuzzy import process, fuzz

from hyio import csv
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

#------------------------------------------------------------
# Options
#------------------------------------------------------------

year_start = 2001
year_end = 2015


#------------------------------------------------------------
# Folders
#------------------------------------------------------------

source_file = os.path.abspath(__file__)

FROOT = os.path.join(os.path.dirname(source_file), '..')

FDATA = os.path.join(FROOT, 'data')
if not os.path.exists(FDATA): os.mkdir(FDATA)

#------------------------------------------------------------
# Get data
#------------------------------------------------------------

fcsv = os.path.join(FDATA, 'HSC_stats_{0}-{1}.csv'.format(year_start,
                                                    year_end))
data, _ = csv.read_csv(fcsv)


fsyl = os.path.join(FDATA, 'HSC_syllabus.csv')
syllabus, _ = csv.read_csv(fsyl)
syllabus['process'] = syllabus['course'].apply(lambda x: x.lower())

languages = ['french', 'chinese', 'arabic', 'armenian',
            'modern', 'classical greek', 'croatian', 'czech', 'dutch', 'estonian',
            'filipino', 'german', 'hindi', 'hungarian', 'indonesian',
            'italian', 'japanese', 'khmer', 'korean', 'latin', 'hebrew',
            'persian', 'portuguese', 'russian', 'spanish', 'swedish',
            'tamil', 'turkish', 'malay', 'macedonian', 'maltese', 'latvian',
            'slovenian', 'vietnamese', 'lithuanian', 'serbian', 'ukrainian',
            'bengali', 'braille', 'polish', 'classical hebrew', 'modern greek']

#------------------------------------------------------------
# Get data
#------------------------------------------------------------

courses = {}
nm = pd.DataFrame({'course':data['Long name'].unique()})
nm['process'] = nm['course'].apply(lambda x: re.sub(' \(.*|language/advanced', '', x.lower()))
nm['process'] = nm['process'].apply(lambda x: re.sub(' +', ' ', re.sub(',|-', ' ', x.lower())))

nm['syllabus'] = ''
nm['syllabus_course'] = ''
nm['syllabus_course_match'] = 0
nm['language'] = ''

language_regexp = '|'.join(['^(heritage |){0}'.format(l) for l in languages])

for idx, row in nm.iterrows():
    print('.. dealing with row {0:3d}/{1:3d} ..'.format(idx+1, nm.shape[0]))

    course_name = row['process']

    # Check if the course is language
    se = re.search(language_regexp, row['process'])
    if se:
        nm.loc[idx, 'language'] = se.group()
        course_name = re.sub(language_regexp, 'languages', course_name)

        if re.search('heritage', course_name):
            course_name = 'languages heritage'

    # Check if the course is life skills
    if re.search('life skills', course_name):
        course_name = 'life skills'

    snm = process.extractOne(course_name, syllabus['process'],
            scorer = fuzz.token_set_ratio)

    if snm[1] > 80:
        nm.loc[idx, 'syllabus_course'] = snm[0]
        nm.loc[idx, 'syllabus_course_match'] = snm[1]

        kk = np.where(syllabus['process'] == snm[0])[0][0]
        nm.loc[idx, 'syllabus'] = syllabus.iloc[kk, 1]


fcsv = os.path.join(FDATA, 'HSC_stats_courses.csv')
comment = 'Course names and syllabus'
nm = nm.sort(['syllabus', 'syllabus_course'])
#nm = nm.sort(['syllabus_course_match'])
csv.write_csv(nm, fcsv, comment, source_file, compress=False)


print('\n\n## Script run completed at {0} ##\n\n'.format(time_now()))
