#!/usr/bin/env python

# -- Script Meta Data --
# Author : J. Lerat, EHP, Bureau of Meteorogoloy
# Versions :
#    V00 - Script written from template on 2016-03-01 21:26:09.878951
#
# ------------------------------


from datetime import datetime
time_now = datetime.now
print('\n\n## Script run started at {0} ##\n\n'.format(time_now()))

import sys, os, re, json, math, subprocess
from string import ascii_letters as letters

import numpy as np
import pandas as pd

from hyio import csv
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from hyplot import putils
from hystat.linreg import Linreg

#------------------------------------------------------------
# Options
#------------------------------------------------------------

year_start = 2001
year_end = 2015

#------------------------------------------------------------
# Folders
#------------------------------------------------------------

source_file = os.path.abspath(__file__)

FROOT = os.path.join(os.path.dirname(source_file), '..')

FDATA = os.path.join(FROOT, 'data')
FIMG = os.path.join(FROOT, 'images')

#------------------------------------------------------------
# Get data
#------------------------------------------------------------

fcsv = os.path.join(FDATA, 'HSC_stats_{0}-{1}.csv'.format(year_start,
                                                    year_end))
data, _ = csv.read_csv(fcsv)

fcsv = os.path.join(FDATA, 'HSC_stats_courses.csv')
courses, _ = csv.read_csv(fcsv)
courses = courses.rename(columns={'course':'Long name'})
data = pd.merge(data, courses, on='Long name', how='left')

fcsv = os.path.join(FDATA, 'HSC_headcount_{0}-{1}.csv'.format(year_start,
                                                    year_end))
count, _ = csv.read_csv(fcsv)
idx = count['variable'] == 'Higher School Certificate'
count = pd.pivot_table(count[idx], index='year', columns='gender',
        values='value')

#------------------------------------------------------------
# Plot
#------------------------------------------------------------

idx = pd.notnull(data['syllabus'])
syllabus = data.loc[idx, 'syllabus'].unique()

plt.close('all')
fig, axs = plt.subplots(2, 5)
axs = axs.flat[:]

for i, sy in enumerate(syllabus):
    idx = data['syllabus'] == sy
    cc = [cn for cn in data.columns if re.search('Total|year', cn)]
    nb = data.loc[idx, cc]
    nb = nb.groupby(nb['year']).sum()
    nb.columns = [re.sub('.* - ', '', cn) for cn in nb.columns]

    # Convert to percent
    perc = nb/count * 100

    # plot
    ax = axs[i]
    putils.set_color_cycle(ax)
    perc.plot(ax=ax, lw=3, legend=(i==0))
    ax.set_title('({0}) {1}'.format(letters[i], sy))
    ax.set_ylabel('% of HSC candidates')
    ax.set_xticklabels(np.arange(2002, 2016, 2))

for i in range(len(syllabus), len(axs)):
    axs[i].axis('off')


fig.set_size_inches((30, 10))
fig.tight_layout()
fp = os.path.join(FIMG, 'HSC_stats_syllabus.png')
fig.savefig(fp)


print('\n\n## Script run completed at {0} ##\n\n'.format(time_now()))
