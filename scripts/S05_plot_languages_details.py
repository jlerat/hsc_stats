#!/usr/bin/env python

# -- Script Meta stats --
# Author : J. Lerat, EHP, Bureau of Meteorogoloy
# Versions :
#    V00 - Script written from template on 2016-03-01 21:26:09.878951
#
# ------------------------------


from datetime import datetime
time_now = datetime.now
print('\n\n## Script run started at {0} ##\n\n'.format(time_now()))

import sys, os, re, json, math, subprocess
from string import ascii_letters as letters

import numpy as np
import pandas as pd

from hyio import csv
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from hyplot import putils

#------------------------------------------------------------
# Options
#------------------------------------------------------------

year_start = 2001
year_end = 2015

#------------------------------------------------------------
# Folders
#------------------------------------------------------------

source_file = os.path.abspath(__file__)

FROOT = os.path.join(os.path.dirname(source_file), '..')

FDATA = os.path.join(FROOT, 'data')
FIMG = os.path.join(FROOT, 'images')

#------------------------------------------------------------
# Get stats
#------------------------------------------------------------

# Stats
fcsv = os.path.join(FDATA, 'HSC_stats_{0}-{1}.csv'.format(year_start,
                                                    year_end))
stats, _ = csv.read_csv(fcsv)
stats = stats.rename(columns = {'Long name': 'course'})

# Course names
fcsv = os.path.join(FDATA, 'HSC_stats_courses.csv')
courses, _ = csv.read_csv(fcsv)
stats = pd.merge(stats, courses, on='course', how='left')

# Head counts
fcsv = os.path.join(FDATA, 'HSC_headcount_{0}-{1}.csv'.format(year_start,
                                                    year_end))
count, _ = csv.read_csv(fcsv)
idx = count['variable'] == 'Higher School Certificate'
count = pd.pivot_table(count[idx], index='year', columns='gender',
        values='value')
count = count['Total']


#------------------------------------------------------------
# Plot
#------------------------------------------------------------

languages = ['french', 'chinese', 'spanish',
                'german', 'japanese']

plt.close('all')
fig, axs = plt.subplots(nrows=2, ncols=5)
axs = axs.T.flat[:]

for i, lan in enumerate(languages):

    idx = stats['language'] == lan

    for ig, gender in enumerate(['Male', 'Female']):

        # Create data table
        st = pd.pivot_table(stats[idx], index='year', columns='syllabus_course',
                values='Total - {0}'.format(gender))

        # Polish column headers
        st.columns = [re.sub('languages|courses|speakers', '', cn).strip().capitalize()
                                for cn in st.columns]

        if not 'Background' in st.columns:
            st['Background'] = 0

        st = st.loc[:, np.sort(st.columns)]

        # Draw plot
        ax = axs[2*i+ig]
        putils.set_color_cycle(ax)

        has_legend = (i==0 and ig == 0)
        pl = st.plot(ax=ax, lw=3, legend = has_legend)

        if has_legend:
            pl.legend(loc=2, frameon=False)

        title = '({0}) {1} - {2}'.format(letters[i+ig], lan.capitalize(), gender)
        ax.set_title(title)


fig.set_size_inches((22, 10))
fig.tight_layout()
fp = os.path.join(FIMG, 'HSC_stats_languages_details.png')
fig.savefig(fp)


print('\n\n## Script run completed at {0} ##\n\n'.format(time_now()))
