#!/usr/bin/env python

# -- Script Meta Data --
# Author : J. Lerat, EHP, Bureau of Meteorogoloy
# Versions :
#    V00 - Script written from template on 2016-03-01 21:26:09.878951
#
# ------------------------------


from datetime import datetime
time_now = datetime.now
print('\n\n## Script run started at {0} ##\n\n'.format(time_now()))

import sys, os, re, json, math, subprocess

import numpy as np
import pandas as pd

from hyio import csv
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

#------------------------------------------------------------
# Options
#------------------------------------------------------------

year_start = 2001
year_end = 2015


#------------------------------------------------------------
# Folders
#------------------------------------------------------------

source_file = os.path.abspath(__file__)

FROOT = os.path.join(os.path.dirname(source_file), '..')

FDATA = os.path.join(FROOT, 'data')
if not os.path.exists(FDATA): os.mkdir(FDATA)

#------------------------------------------------------------
# Get data
#------------------------------------------------------------

fcsv = os.path.join(FDATA, 'HSC_stats_{0}-{1}.csv'.format(year_start,
                                                    year_end))
data, _ = csv.read_csv(fcsv)


#------------------------------------------------------------
# Get data
#------------------------------------------------------------

courses = {}
nm = pd.DataFrame({'course':data['Long name'].unique()})
nm['process'] = nm['course'].apply(lambda x: re.sub(' \(.*', '', x.lower()))
nm['domain'] = ''
nm['sub-domain'] = ''
nm['extension'] = False

domains = {
        'Mathematics':['mathematics'],
        'Agriculture':['agriculture', 'forest', 'rural', 'horticulture', 'beef',
                'equine', 'grain', 'horse', 'meat', 'wool', 'animal',
                'aquaculture', 'revegetation'],
        'Child':['child', 'childhood'],
        'Hospitality':['food', 'hospitality', 'tourism', 'catering', 'wine',
            'baking', 'holiday'],
        'Health': ['aged care', 'dental', 'health', 'nursing'],
        'IT':['information technology', 'digital technology',
            'computing', 'software', 'logo', 'computer',
            'information processes'],
        'Law': ['legal'],
        'Teaching': ['teaching', 'education'],
        'Mechanic and Construction':['automotive', 'plumbing', 'construction',
                'furniture', 'carpenter', 'electrotechnology',
                'carpentry', 'metal', 'lining', 'furnishing', 'motorcycle',
                'vehicle', 'manufacturing', 'bicycles', 'tiling', 'floor',
                'building', 'trade', 'electrical'],
        'Aboriginal':['aboriginal', 'indigenous'],
        'Beauty': ['beauty', 'cosmetics', 'hairdressing', 'makeup'],
        'Economics':['economics', 'business', 'management', 'logistics',
            'accounting', 'finance', 'financial', 'marketing'],
        'Humanities': ['psychology', 'geography', 'philosophy', 'history',
            'religion', 'anthropology', 'politic', 'catholic', 'biblical',
            'sociology'],
        'Sport':['sport', 'outdoor', 'physical education', 'ballet', 'fitness'],
        'English':['english'],
        'Science - Core':['biology', 'chemistry', 'physics'],
        'Science - Peripheral':['earth', 'cosmology', 'environmental science',
                    'bioscience', 'perspective in science', 'science as',
                    'communication in science', 'science', 'scientific',
                    'sustainable'],
        'Language':['french', 'chinese', 'arabic', 'armenian',
            'greek', 'croatian', 'czech', 'dutch', 'estonian',
            'filipino', 'german', 'hindi', 'hungarian', 'indonesian',
            'italian', 'japanese', 'khmer', 'korean', 'latin', 'hebrew',
            'persian', 'portuguese', 'russian', 'spanish', 'swedish',
            'tamil', 'turkish', 'malay', 'macedonian', 'maltese', 'latvian',
            'slovenian', 'vietnamese', 'lithuanian', 'serbian', 'ukrainian',
            'bengali', 'braille', 'polish'],
        'Creative': ['design', 'dance', 'drama', 'media', 'music',
            'visual arts', 'performing arts', 'entertainment', 'painting',
            'fashion', 'applied arts', 'ceramic', 'creative arts', 'digital imaging']
    }

nm['domain'] = 'Undefined'
nm['sub-domain'] = 'undefined'

for d in domains:
    for k in domains[d]:
        idx = nm['process'].apply(lambda x: bool(re.search(k, x)))
        nm.loc[idx, 'domain'] = d
        nm.loc[idx, 'sub-domain'] = k

idx = nm['process'].apply(lambda x: bool(re.search('extension', x)))
nm.loc[idx, 'extension'] = True

nm = nm.sort('domain')
nocat = np.sum(nm['domain'] == 'Undefined')
print('No domain : {0}'.format(nocat))

fcsv = os.path.join(FDATA, 'HSC_stats_courses.csv')
comment = 'Course names and mapping to domain'
csv.write_csv(nm, fcsv, comment, source_file, compress=False)


print('\n\n## Script run completed at {0} ##\n\n'.format(time_now()))
